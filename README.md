![Screenshot preview of the theme "Punchline" by glenthemes](https://64.media.tumblr.com/67a23d884b36251b0f6a78675690e4af/a318d76c06cf22eb-f4/s1280x1920/24b0c829c9b4b9e680b9aa0124c5d7ee912ac0f1.gif)

**Theme no.:** 21  
**Theme name:** Punchline  
**Theme type:** Free / Tumblr use  
**Description:** Initially released in 2016, I finally decided to give this theme a makeover and recoded it from scratch. Still features Vi from League of Legends, but with her PsyOps skin this time.  
**Author:** @&hairsp;glenthemes  

**Release date:** [2016-12-23](https://64.media.tumblr.com/ba26fa394ffd3a61624a1a7c903249fd/tumblr_oin71o1SOD1ubolzro1_540.gif)  
**Rework date:** 2021-02-20

**Post:** [glenthemes.tumblr.com/post/643622443342266368](https://glenthemes.tumblr.com/post/643622443342266368)  
**Preview:** [glenthpvs.tumblr.com/punchline](https://glenthpvs.tumblr.com/punchline)  
**Download:** [pastebin.com/SMJhYyPD](https://pastebin.com/SMJhYyPD)  
**Credits:** [glencredits.tumblr.com/punchline](https://glencredits.tumblr.com/punchline)
