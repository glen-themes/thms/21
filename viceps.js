// the whole theme is barely held together by hopes and dreams

$(document).ready(function(){
    $(".tumblr_preview_marker___").remove();

    var customize_page = window.location.href.indexOf("/customize") > -1;
    var on_main = window.location.href.indexOf("/customize") < 0;

    var root = document.documentElement;
    
    // load 'reblog' and 'like' SVGs
    fetch("//glen-assets.github.io/core-svgs/freepik_repeat.html")
    .then(rt_svg => {
      return rt_svg.text()
    })
    .then(rt_svg => {
      $(".retweet").html(rt_svg)
    });

    fetch("//glen-assets.github.io/core-svgs/freepik_like.html")
    .then(like_svg => {
      return like_svg.text()
    })
    .then(like_svg => {
      $(".heart").html(like_svg)
    });


    // if:reblog dividers, increase space between 1st commenter & the next
    var p_gap = getComputedStyle(document.documentElement)
               .getPropertyValue("--Paragraph-Margins"),
        numonly = parseFloat(p_gap),
        suffix = p_gap.split(/\d/).pop();

    $(".oui p").each(function(){
        if($(this).parents(".comment_container").next(".comment_container").length){
            $(this).css("margin-bottom",numonly / 2 + suffix)
        }
    });


    $(".oui").each(function(){
        // remove extra space from first commenter
        // if there is nothing before it
        if(!$(this).find(".comment_container:first").closest(".body").length){
            $(this).find(".comment_container:first").addClass("ccfirst")

        };

        // remove reblog border from last commenter
        $(this).find(".comment_container:last").addClass("cclast");
    });


    $(".posts").each(function(){
        $(this).find(".comment_container:last").addClass("cclast")
    });


    // turn npf_chat into a chat post
    $(".npf_chat").addClass("chatholder").removeClass("npf_chat");

    $(".chatholder").each(function(){
        if($(this).attr("data-npf")){
            $(this).find("b").addClass("chat_label");
            $(this).contents().last().wrap("<span class='chat_content'>")
        }
    });
    // add colon at the end of each chat_label
    $(".chat_label").each(function(){
        if(!$(this).text().endsWith(":")){
            $(this).append(":")
        }
    });

    $(".posts").each(function(){
        // show source if post does not have caption
        if(!$(this).find(".comment_container").length){
            $(this).find(".nosrc").css("display","flex")
        }
    });

    $(".nosrc").each(function(){
        if($(this).is(":hidden")){
            $(this).remove()
        }
    });

    $(".body").each(function(){
        if(!$(this).prev().length){
            $(this).find(".comment_container:first").css("margin-top","calc(var(--Captions-Gap) / -2)")
        }
    });


    $(".npf_inst").each(function(){
        if($(this).parent().prev(".commenter")){
            $(this).css("margin-top","var(--NPF-Caption-Spacing)")
        }
    });


    $(".posts").each(function(){
        var fc = $(this).find(".comment_container").eq(0);
        var fcinst = fc.find(".npf_inst");
        if(fcinst.length){
            if(!fcinst.prev().length){
                if(fcinst.next().length){
                    fcinst.prependTo($(this));
                    fcinst.css("margin-bottom","var(--NPF-Caption-Spacing)")
                    fc.css("margin-top","")
                }
            }
        }

        if($(this).children().first().is(".npf_inst")){
            $(this).children().first().css("margin-top","")
        }
    });


    $(".caption").each(function(){
        // remove empty captions
        if(!$(this).children().length){
            $(this).remove();
        }
    });

    $(".posts br").each(function(){
        // remove unnecessary <br>s
        if(!$(this).prev().length){
            $(this).remove();
        }
    });

    $(".ccfirst").each(function(){
        if(!$(this).closest(".caption").length){
            if(!$(this).prev().length){
                $(this).css("margin-top","calc(var(--Captions-Gap) / -2)")
            }
        }
    });

    $(".commenter img").each(function(){
        $(this).wrap("<div class='sfr'>");

        if(customize_page){
            if($(this).attr("src") == "https://assets.tumblr.com/images/default_avatar/cube_closed_64.png"){
                $(this).hide()
            }
        }
    });

    // remove 'deactivated' string from reblogger
    $(".commenter a").each(function(){
        var check_d = $(this).text();
        if(check_d.indexOf("-deactivated2") > -1){
            var lastdas = check_d.lastIndexOf("-");
            var gen_name = check_d.substring(0,lastdas);

            $(this).text(gen_name)
        }
    });

    // get line-height and set this as bullet points' height
    var LH = getComputedStyle(document.documentElement)
               .getPropertyValue("--Text-LineHeight"),
        LH_num = parseFloat(LH);

    root.style.setProperty('--LH', LH_num);

    // remove comma from notecount
    $(".permaleft a").each(function(){
        var nc = $(this).html().replace(",","");
        $(this).html(nc)
    });

    // turn allcaps tags into beaufort font
    $(".posts a, .posts b, .posts i, .posts span, .posts p, .posts big, .posts small").each(function(){
        var thistext = $(this).text();
        if(thistext === thistext.toUpperCase()){
            $(this).addClass("allcaps")
        }
    });

    // wrap audio post album image with shadow
    $(".a-lbum-img").each(function(){
        $("<div class='vingt'>").insertAfter($(this));
    });

    // attempt to stop awkward scrollbar disappearance on lightbox click
    $(".npf_inst img, .photoset-grid img").click(function(){
        if($(document).height() > $(window).height()){
            $("body").addClass("plsstop")
        }
    });

    // Do Things if hover!desc is turned on
    if($(".descbox").attr("hover") == "dhov"){
        $(".descbox").addClass("deschov");
        $(".chev").addClass("chevhov");
        $(".topleft").addClass("tophov");
    }

    /*---then---*/

    $(".deschov").hover(function(){
        $(".chevhov").addClass("dothething").removeClass("wayback");
        $(".deschov").addClass("dix").removeClass("okboomer");
    }, function(){
        $(".chevhov").removeClass("dothething").addClass("wayback");
        $(".deschov").removeClass("dix").addClass("okboomer")
    });

    /*--------*/

    $(".tophov").hover(function(){
        $(".deschov").addClass("dix").removeClass("okboomer");
        $(".chevhov").addClass("dothething").removeClass("wayback");
    }, function(){
        $(".deschov").removeClass("dix").addClass("okboomer");
        $(".chevhov").removeClass("dothething").addClass("wayback");
    });

    // attempt to preload background image (.wallpaper)
    var bgimage = new Image();
    bgimage.src = $(".wallpaper").css("background-image").replace(/(^url\()|(\)$|[\"\'])/g,'');
    $(bgimage).load(function(){
        $(".wallpaper").fadeIn(420);
     });


     // BACKTOTOP BUTTON
    $(window).scroll(function(){
        if($(document).scrollTop() > $(document).height() * 0.069){
            $(".tangerine").fadeIn(269);
        } else {
            $(".tangerine").fadeOut(269);
        }
    });

    $(".tangerine").click(function(){
        if(customize_page){
            $("body").animate({
                scrollTop:0
            },900);
        }

        if(on_main){
            $("html").animate({
                scrollTop:0
            },900);
        }
    });

    // clicktags
    $(".viewtags").click(function(){
        $(this).parents(".permarow").prev(".tagcont").slideToggle();
    });

    // remove tumblr redirects script by magnusthemes@tumblr
    // part 1/2
    $('a[href*="t.umblr.com/redirect"]').each(function(){
      var originalURL = $(this).attr("href").split("?z=")[1].split("&t=")[0];
      var replaceURL = decodeURIComponent(originalURL);
      $(this).attr("href", replaceURL);
    });

    // part 2/2
    function noHrefLi(){
        var linkSet = document.querySelectorAll('a[href*="href.li/?"]');
        Array.prototype.forEach.call(linkSet,function(el,i){
            var theLink = linkSet[i].getAttribute('href').split("href.li/?")[1];
            linkSet[i].setAttribute("href",theLink);
        });
    }
    noHrefLi();

    // turn linkhost into @BLOGNAME if host is a tumblr user
    $(".genlinkbase").each(function(){
        getcon = $(this).text();

        if(getcon.indexOf(".tumblr.com") > -1){
            var ssw = getcon.substr(0,getcon.indexOf('.tumblr.com'));
            var n = ssw.lastIndexOf('/');
            var result = ssw.substring(n + 1);
            $(this).html("<span class ='et'>@</span>" + result)
        }
    });

    // wrap and remove duplicate quote source
    $(".quote-source, .quote-source p").each(function(){
        $(this).contents().filter(function(){
            return this.nodeType === 3;
        }).wrap('<span/>');

        $(this).find(".tumblr_blog").remove();
    });

    $(".quote-source span").each(function(){
        var viatxt = $(this).text();

        if(viatxt.indexOf("(Source: ") > -1){
            $(this).remove();
        }

        if(viatxt.indexOf(")") > -1){
            if(viatxt.length == "1"){
                $(this).remove()
            }
        }

        if(viatxt.endsWith(" (via ")){
            var v = viatxt.slice(0,-6);
            $(this).text(v)
        }
    });

    $(".quote-source a").each(function(){
        if(!$(this).hasClass("tumblr_blog")){
            if(!$(this).siblings("span").length){
                $(this).remove();
            }
        }
    });

    // remove empty quote source
    $(".quote-source p").each(function(){
        if(!$(this).children().length){
            $(this).remove();
        }
    });

    // if post starts with a heading, turn the heading into a h1 post
    $(".posts h2").each(function(){
        if(!$(this).prev().length){
            if($(this).parent().prev().is(".commenter")){
            $(this).replaceWith('<h1 class="h1c">' + $(this).html() + '</h1>')
            }
        }
    });

	$(".body").each(function(){
		if(!$(this).children().siblings().length){
			$(this).children().last().css("margin-bottom","var(--Post-Padding)")
		}
	});

	if(on_main){
		$(window).load(function(){
			$(".wallpaper").fadeIn(420);
		});
	}
});//end ready
